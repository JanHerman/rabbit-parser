<?php

namespace App\Commands;

use App\Consumers\ConsumerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class StatsXMLConsumer extends Command
{
    /** @var \App\Consumers\ConsumerInterface */
    private $consumer;

    /**
     * StatsXMLConsumer constructor.
     *
     * @param ConsumerInterface $consumer
     *
     * @throws \Symfony\Component\Console\Exception\LogicException
     */
    public function __construct(ConsumerInterface $consumer)
    {
        parent::__construct(null);
        $this->consumer = $consumer;
    }

    /**
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure(): void
    {
        $this->setName('parser:xml-stat-consumer');
        $this->setDescription('Consumer:');
        $this->addOption('setup', 's', InputOption::VALUE_OPTIONAL, 'Setup connection?');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->consumer->setOutput($output)->consume();
    }

}