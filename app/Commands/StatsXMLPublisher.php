<?php

namespace App\Commands;

use App\Parsers\ParserInterface;
use App\Processors\ProcessorInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class StatsXMLPublisher extends Command
{
    /**
     * @var ProcessorInterface
     */
    private $processor;

    /**
     * @var ParserInterface
     */
    private $parser;

    /**
     * StatsXMLPublisher constructor.
     *
     * @param ProcessorInterface $processor
     * @param ParserInterface $parser
     *
     * @throws \Symfony\Component\Console\Exception\LogicException
     */
    public function __construct(ProcessorInterface $processor, ParserInterface $parser)
    {
        parent::__construct(null);
        $this->processor = $processor;
        $this->parser = $parser;
    }

    /**
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function configure(): void
    {
        $this->setName('parser:xml-stat-processor');
        $this->setDescription('TODO:');
        $this->addOption('file', 'f', InputOption::VALUE_REQUIRED, 'File to parse');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $file = $input->getOption('file');

        if (empty($file) || !file_exists($file)) {
            throw new InvalidArgumentException('Wrong parse file');
        }

        $callback = function ($data) {
            return $this->processor->process($data);
        };

        $this->parser->parse($file, $callback);
    }

}