<?php

namespace App\Consumers;

use Symfony\Component\Console\Output\OutputInterface;

interface ConsumerInterface
{
    public function consume(): void;

    /**
     * @param string $msg
     *
     * @return void
     */
    public function output(string $msg): void;

    /**
     * @param \Symfony\Component\Console\Output\OutputInterface $outputInterface
     *
     * @return \App\Consumers\ConsumerInterface
     */
    public function setOutput(OutputInterface $outputInterface): self;

}