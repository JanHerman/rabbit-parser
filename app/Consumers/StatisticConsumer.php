<?php

namespace App\Consumers;

use PhpAmqpLib\Message\AMQPMessage;

class StatisticConsumer extends MyConsumerAbstract
{
    /**
     * @throws \PhpAmqpLib\Exception\AMQPOutOfBoundsException
     * @throws \PhpAmqpLib\Exception\AMQPRuntimeException
     */
    public function consume(): void
    {
        $this->output(" [*] Waiting for messages. To exit press CTRL+C\n");

        $callback = function (AMQPMessage $msg) {
            $json = json_decode($msg->body, true);

            $model = $this->repository->createAndReturn($json);
            $this->repository->save($model);

            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        };

        $this->channel->basic_consume($this->queueName, '', false, false, false, false, $callback);

        while (\count($this->channel->callbacks)) {
            $this->channel->wait();
        }

        $this->channel->close();
        $this->connection->close();
    }

}