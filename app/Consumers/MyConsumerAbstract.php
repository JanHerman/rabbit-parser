<?php

namespace App\Consumers;

use App\Publishers\FanoutPublisher;
use App\Repositories\RepositoryInterface;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Symfony\Component\Console\Output\OutputInterface;

abstract class MyConsumerAbstract implements ConsumerInterface
{
    /** @var AMQPStreamConnection */
    protected $connection;

    /** @var  \PhpAmqpLib\Channel\AMQPChannel */
    protected $channel;

    /** @var OutputInterface */
    private $outputInterface;

    /** @var \App\Repositories\RepositoryInterface */
    protected $repository;

    /** @var string */
    protected $exchangeName;

    /** @var string */
    protected $queueName;

    /**
     * MyConsumerAbstract constructor.
     *
     * @param \PhpAmqpLib\Connection\AMQPStreamConnection $connection
     * @param string $exchangeName
     * @param \App\Repositories\RepositoryInterface $repository
     */
    public function __construct(AMQPStreamConnection $connection, string $exchangeName, RepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->connection = $connection;
        $this->exchangeName = $exchangeName;

        //TODO param z konzole
        if (true) {
            $this->setup($connection);
        }
    }

    /**
     * @param \PhpAmqpLib\Connection\AMQPStreamConnection $connection
     */
    private function setup(AMQPStreamConnection $connection): void
    {
        $this->connection = $connection;
        $this->channel = $connection->channel();

        $this->channel->exchange_declare($this->exchangeName, FanoutPublisher::TYPE, false, false, false);

        [$this->queueName, ,] = $this->channel->queue_declare('', false, false, true, false);

        $this->channel->queue_bind($this->queueName, $this->exchangeName);
    }

    /**
     * @param string $msg
     *
     * @return void
     */
    public function output(string $msg): void
    {
        if ($this->outputInterface) {
            $this->outputInterface->writeln($msg);
        }
    }

    /**
     * @param \Symfony\Component\Console\Output\OutputInterface $outputInterface
     *
     * @return \App\Consumers\ConsumerInterface
     */
    public function setOutput(OutputInterface $outputInterface): ConsumerInterface
    {
        $this->outputInterface = $outputInterface;

        return $this;
    }
}