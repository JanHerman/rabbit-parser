<?php

namespace App\Repositories;

use App\Factory\FactoryInterface;
use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Eloquent\Model;

class StatsRepository implements RepositoryInterface
{
    /**
     * @var Manager
     */
    private $capsule;

    /**
     * @var \App\Factory\FactoryInterface
     */
    private $factory;

    /**
     * StatsRepository constructor.
     *
     * @param \Illuminate\Database\Capsule\Manager $capsule
     * @param \App\Factory\FactoryInterface $factory
     */
    public function __construct(Manager $capsule, FactoryInterface $factory)
    {
        $this->capsule = $capsule;
        $this->factory = $factory;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     */
    public function save(Model $model): void
    {
        $model->save();
    }

    /**
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createAndReturn(array $data): Model

    {
        return $this->factory->createAndReturn($data);
    }
}