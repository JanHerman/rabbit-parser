<?php

namespace App\Repositories;

use App\Factory\FactoryInterface;
use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Eloquent\Model;

interface RepositoryInterface
{
    /**
     * StatsRepository constructor.
     *
     * @param \Illuminate\Database\Capsule\Manager $capsule
     * @param \App\Factory\FactoryInterface $factory
     */
    public function __construct(Manager $capsule, FactoryInterface $factory);

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     */
    public function save(Model $model): void;

    /**
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createAndReturn(array $data): Model;

}