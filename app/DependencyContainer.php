<?php

namespace App;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class DependencyContainer implements ContainerInterface
{
    /** @var */
    protected $definitions;

    /**
     * Finds an entry of the container by its identifier and returns it.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @throws NotFoundExceptionInterface  No entry was found for **this** identifier.
     * @throws ContainerExceptionInterface Error while retrieving the entry.
     *
     * @return mixed Entry.
     * @throws \UnexpectedValueException
     */
    public function get($id)
    {
        if ($this->has($id)) {
            return $this->definitions[$id]();
        }
        throw new \UnexpectedValueException('Container does not exist');
    }

    /**
     * @param string $id
     * @param callable $service
     *
     * @throws \UnexpectedValueException
     */
    public function set(string $id, callable $service): void
    {
        if (isset($this->definitions[$id])) {
            throw new \UnexpectedValueException('Container already exist');
        }

        $this->definitions[$id] = $service;
    }

    /**
     * Returns true if the container can return an entry for the given identifier.
     * Returns false otherwise.
     *
     * `has($id)` returning true does not mean that `get($id)` will not throw an exception.
     * It does however mean that `get($id)` will not throw a `NotFoundExceptionInterface`.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @return bool
     */
    public function has($id): bool
    {
        return isset($this->definitions[$id]);
    }
}