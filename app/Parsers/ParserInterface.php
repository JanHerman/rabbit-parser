<?php

namespace App\Parsers;


interface ParserInterface
{
    /**
     * @param string $content
     * @param callable $callback
     */
    public function parse(string $content, callable $callback): void;
}