<?php

namespace App\Parsers;

interface StatsXMLParseInterface
{
    /**
     * @param string $name
     * @param array $attributesArray
     */
    public function help(string $name, array $attributesArray): void;

    /**
     * @return array
     */
    public function getParsedData(): array;
}