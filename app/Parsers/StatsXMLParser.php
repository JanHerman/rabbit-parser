<?php

namespace App\Parsers;

class StatsXMLParser implements ParserInterface
{
    protected const ENCODING = 'UTF-8';
    protected const HANDLER_START = 'startTag';
    protected const HANDLER_END = 'endTag';

    /** @var array */
    private $stack = [];

    /** @var resource */
    private $parser;

    /** @var string */
    private $current;

    /** @var \App\Parsers\StatsXMLParseInterface */
    private $helperObj;

    /**
     * StatsXMLParser constructor.
     *
     * @param \App\Parsers\StatsXMLParseInterface $statsXMLParserHelper
     */
    public function __construct(StatsXMLParseInterface $statsXMLParserHelper)
    {
        $this->parser = xml_parser_create(self::ENCODING);
        $this->helperObj = $statsXMLParserHelper;

        xml_set_object($this->parser, $this);
        xml_set_element_handler($this->parser, self::HANDLER_START, self::HANDLER_END);
    }

    /**
     * @param resource $parser
     * @param string $name
     * @param array $attributesArray
     */
    private function startTag($parser, string $name, array $attributesArray): void
    {
        $this->helperObj->help($name, $attributesArray);
        $this->current = $name;

        call_user_func($this->callback, $this->helperObj->getParsedData());
    }

    /**
     * @param resource $parser
     * @param string $name
     */
    private function endTag($parser, string $name): void
    {
        $this->current = array_pop($this->stack);
    }

    /**
     * @param string $content
     *
     * @return void
     * @throws \UnexpectedValueException
     */
    public function parse(string $content, callable $callback): void
    {
        $fh = fopen($content, 'rb');
        $this->callback = $callback;

        if (false === $fh) {
            throw new \UnexpectedValueException('Couldnt open file');
        }

        while (!feof($fh)) {
            $data = fread($fh, 4096);
            xml_parse($this->parser, $data, feof($fh));

        }
    }
}