<?php

namespace App\Parsers;

class StatsXMLParserHelper implements StatsXMLParseInterface
{
    protected const PLAYER_ID = 'PLAYER-ID';
    protected const CATEGORY = 'CATEGORY';
    protected const RANKING = 'RANKING';
    protected const CATEGORY_ID = 'CATEGORY-ID';
    protected const ID = 'ID';

    /** @var array */
    private $tempNodeDataArray = [];

    /** @var array */
    private $parsedDataArray = [];

    /** @var array */
    private $attributesArray = [];

    /** @var int */
    private $currentId;

    /** @var int */
    private $categoryId;

    /** @var bool */
    private $initParsing = false;

    /**
     * @param string $name
     * @param array $attributesArray
     */
    public function help(string $name, array $attributesArray): void
    {
        $this->parsedDataArray = [];
        $result = false;
        $this->attributesArray = $attributesArray;

        if ($name === self::PLAYER_ID) {
            $result = $this->checkPlayerId();
        }

        if ($name === self::CATEGORY) {
            $result = $this->checkCategoryId();
        }

        if ($name === self::RANKING) {
            $result = $this->setNodeData();
        }

        if (!$result) {
            $this->setData($name);
        }
    }

    /**
     * @param string $name
     */
    private function setData(string $name): void
    {
        if ($this->initParsing && \count($this->attributesArray)) {
            $this->tempNodeDataArray[$name] = $this->attributesArray;
        }
    }

    /**
     * @return bool
     */
    private function checkPlayerId(): bool
    {
        if (\count($this->attributesArray)) {
            $this->currentId = (int)$this->attributesArray[self::ID];

            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    private function checkCategoryId(): bool
    {
        if (\count($this->attributesArray)) {
            $this->categoryId = (int)$this->attributesArray[self::CATEGORY_ID];

            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    private function setNodeData(): bool
    {
        if (!$this->initParsing) {
            $this->initParsing = true;
        }

        if ($this->currentId && $this->categoryId && !\count($this->attributesArray) && \count($this->tempNodeDataArray)) {
            $this->parsedDataArray[$this->categoryId][$this->currentId] = $this->tempNodeDataArray;
            $this->tempNodeDataArray = [];
            $this->currentId = null;

            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public function getParsedData(): array
    {
        return $this->parsedDataArray;
    }
}