<?php

namespace App\Factory;

use App\Models\Player;
use Illuminate\Database\Eloquent\Model;

class SportFactory implements FactoryInterface
{

    /**
     * @param array $data
     *
     * @return \App\Models\Player
     * @throws \UnexpectedValueException
     */
    public function createAndReturn(array $data): Model
    {
        if (!isset($data['NAME']['DISPLAY-NAME'])) {
            throw new \UnexpectedValueException('No name');
        }

        $attributes['name'] = $data['NAME']['DISPLAY-NAME'];

        return new Player($attributes);
    }
}
