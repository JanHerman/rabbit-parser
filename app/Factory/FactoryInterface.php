<?php

namespace App\Factory;

use Illuminate\Database\Eloquent\Model;

interface  FactoryInterface
{
    /**
     * @param array $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createAndReturn(array $data): Model;
}