<?php

namespace App\Publishers;

use PhpAmqpLib\Message\AMQPMessage;

interface PublisherInterface
{
    /**
     * @param \PhpAmqpLib\Message\AMQPMessage $message
     *
     * @return void
     */
    public function publish(AMQPMessage $message): void;

    public function closeConnection(): void;
}