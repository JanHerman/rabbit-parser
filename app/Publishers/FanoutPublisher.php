<?php

namespace App\Publishers;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class FanoutPublisher implements PublisherInterface
{
    public const TYPE = 'fanout';

    /** @var \PhpAmqpLib\Channel\AMQPChannel */
    private $channel;

    /** @var \PhpAmqpLib\Connection\AMQPStreamConnection */
    private $connection;

    /** @var string */
    private $exchangename;

    /**
     * StatisticPublisher constructor.
     *
     * @param \PhpAmqpLib\Connection\AMQPStreamConnection $connection
     * @param string $exchangename
     */
    public function __construct(AMQPStreamConnection $connection, string $exchangename)
    {
        $this->connection = $connection;
        $this->channel = $connection->channel();
        $this->exchangename = $exchangename;

        $this->channel->exchange_declare($exchangename, self::TYPE, false, false, false);
    }

    /**
     * @param \PhpAmqpLib\Message\AMQPMessage $message
     */
    public function publish(AMQPMessage $message): void
    {
        $this->channel->basic_publish($message, $this->exchangename);
    }

    public function closeConnection(): void
    {
        $this->connection->close();
        $this->channel->close();
    }
}