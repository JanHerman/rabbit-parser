<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name
 */
class Player extends Model
{
    /** @var string  */
    protected $table = 'players';

    /** @var array  */
    protected $fillable = ['name'];

    /** @var bool  */
    public $timestamps = false;

}