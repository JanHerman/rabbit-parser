<?php

include_once __DIR__ . '/../vendor/autoload.php';

$container = new \App\DependencyContainer();
$exchangename = 'mainExchange';

/**
 * @return Generator
 */
function gen_one_to_three() {
    for ($i = 1; $i <= 3; $i++) {
        // Note that $i is preserved between yields.
        yield $i;
    }
}

$generator = gen_one_to_three();


foreach ($generator as $value) {
    echo "$value\n";
}

$container->set(
    'errorHandlerMonolog',
    function () {
        $logger = new \Monolog\Logger('Parse logger');
        \Monolog\ErrorHandler::register($logger);

        $logger->pushHandler(new \Monolog\Handler\StreamHandler('App/Log/log', \Monolog\Logger::DEBUG));

        return $logger;
    }
);

set_exception_handler(
    function (Throwable $e) use ($container) {
        $container->get('errorHandlerMonolog')->log(\Monolog\Logger::DEBUG, $e);
    }
);

set_error_handler(
    function (Throwable $e) use ($container) {
        $container->get('errorHandlerMonolog')->log(\Monolog\Logger::DEBUG, $e);
    }
);

$container->set(
    'config',
    function () {
        return parse_ini_file(__DIR__ . '/../config/config.dev.ini', true);
    }
);

$container->set(
    'eloquent',
    function () {
        $capsule = new Illuminate\Database\Capsule\Manager();

        $capsule->addConnection(
            [
                'driver' => 'mysql',
                'host' => 'mysql',
                'database' => 'xml_example',
                'username' => 'xml_example',
                'password' => 'xml_example',
                'charset' => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix' => '',
            ]
        );

        $capsule->setAsGlobal();
        $capsule->bootEloquent();

        return $capsule;
    }
);

$container->set(
    'AMQPConnection',
    function () use ($container) {
        $config = $container->get('config');

        return new PhpAmqpLib\Connection\AMQPStreamConnection(
            $config['amqp']['host'],
            $config['amqp']['port'],
            $config['amqp']['login'],
            $config['amqp']['passwd']
        );
    }
);

$container->set(
    'statisticsPublisher',
    function () use ($container, $exchangename) {
        return new \App\Publishers\FanoutPublisher(
            $container->get('AMQPConnection'), $exchangename
        );
    }
);

$container->set(
    'statisticsProcessor',
    function () use ($container) {
        return new \App\Processors\StatsXMLProcessor(
            $container->get('statisticsPublisher')
        );
    }
);

$container->set(
    'StatsXMLParserHelper',
    function () {
        return new \App\Parsers\StatsXMLParserHelper();
    }
);

$container->set(
    'statisticsFileParser',
    function () use ($container) {
        return new \App\Parsers\StatsXMLParser($container->get('StatsXMLParserHelper'));
    }
);

$container->set(
    'sportFactory',
    function () {
        return new \App\Factory\SportFactory();
    }
);

$container->set(
    'StatsRepository',
    function () use ($container) {
        return new \App\Repositories\StatsRepository($container->get('eloquent'), $container->get('sportFactory'));
    }
);

$container->set(
    'statisticsConsumer',
    function () use ($container, $exchangename) {
        return new \App\Consumers\StatisticConsumer(
            $container->get('AMQPConnection'),
            $exchangename,
            $container->get('StatsRepository')
        );
    }
);
