<?php

namespace App\Processors;

interface ProcessorInterface
{
    /**
     * @param array $data
     *
     * @return void
     */
    public function process(array $data): void;
}