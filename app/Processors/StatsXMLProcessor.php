<?php

namespace App\Processors;

use App\Publishers\PublisherInterface;
use PhpAmqpLib\Message\AMQPMessage;

class StatsXMLProcessor implements ProcessorInterface
{
    /**
     * @var PublisherInterface
     */
    private $publisher;

    /**
     * StatsXMLProcessor constructor.
     *
     * @param PublisherInterface $publisher
     */
    public function __construct(PublisherInterface $publisher)
    {
        $this->publisher = $publisher;
    }

    /**
     * @param array $data
     *
     * @return void
     */
    public function process(array $data): void
    {

        if(!empty($data)) {
            var_dump($data);
            die;
        } else {
            var_dump('empty');
        }

        return;
        foreach ($data as $categoryKey => $categoryDataArray) {
            foreach ($categoryDataArray as $playerId => $playerNodeArray) {
                $message = json_encode($playerNodeArray);
                $this->publisher->publish(new AMQPMessage($message));
            }
        }
        $this->publisher->closeConnection();
    }
}