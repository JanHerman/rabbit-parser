<?php

include_once __DIR__ . '/app/bootstrap.php';

$app = new \Symfony\Component\Console\Application();

$app->add(
    new \App\Commands\StatsXMLPublisher(
        $container->get('statisticsProcessor'),
        $container->get('statisticsFileParser')
    )
);

$app->add(
    new \App\Commands\StatsXMLConsumer(
        $container->get('statisticsConsumer')
    )
);

$app->run();
