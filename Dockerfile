FROM php:7.2-apache

RUN apt-get update && apt-get install -y apt-utils

RUN apt-get install -y \
       zlib1g-dev libjpeg-dev libpng-dev libsodium-dev unzip

RUN docker-php-ext-install zip pdo_mysql mysqli bcmath sodium

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- \
	--install-dir=/usr/local/bin \
	--filename=composer